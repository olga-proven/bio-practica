biopython==1.82
Flask==3.0.0
gunicorn==20.1.0
tabulate==0.9.0
tqdm==4.66.1
urllib3==2.1.0

