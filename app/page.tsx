'use client';
import seq from "bionode-seq";
import { useEffect, useState } from "react";


export default function () {

  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    fetch('/api/blastp').then(res => res.json()).then(data => {
      setCurrentTime(data.time);
    });
  }, [])


  return (
    <main className="p-6">
      <div className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
      </div>
      <p className="text-center mt-10">The current time is {currentTime}.</p>
    </main>
  );
}
